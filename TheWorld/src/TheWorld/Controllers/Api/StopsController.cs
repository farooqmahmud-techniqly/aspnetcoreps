﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TheWorld.Exceptions;
using TheWorld.Models;
using TheWorld.Services;
using TheWorld.ViewModels;

namespace TheWorld.Controllers.Api
{
    [Authorize]
    public class StopsController : Controller
    {
        private readonly IWorldRepository _worldRepository;
        private readonly IGeoCoordsService _geoCoordsService;
        private readonly ILogger<StopsController> _logger;

        public StopsController(
            IWorldRepository worldRepository,
            IGeoCoordsService geoCoordsService,
            ILogger<StopsController> logger)
        {
            _worldRepository = worldRepository;
            _geoCoordsService = geoCoordsService;
            _logger = logger;
        }

        [HttpGet("api/trips/{tripId}/stops")]
        public async Task<IActionResult> Get(int tripId)
        {
            var username = User.Identity.Name;
            var trip = await _worldRepository.GetTripAsync(username, tripId);

            if (trip == null)
            {
                return NotFound();
            }

            var viewModel = Mapper.Map<TripWithStopsViewModel>(trip);
            viewModel.Links.Self = Url.Link("GetTripById", new { id = tripId });

            return Ok(viewModel);
        }

        [HttpPost("api/trips/{tripId}/stops")]
        public async Task<IActionResult> Post(int tripId, [FromBody] StopViewModel[] stopViewModels)
        {
            if (stopViewModels == null || stopViewModels.Length == 0)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            try
            {
                var stops = Mapper.Map<IEnumerable<Stop>>(stopViewModels).ToList();

                foreach (var stop in stops)
                {
                    var geoCoordsServiceResult = await _geoCoordsService.GetCoordsAsync(stop.Name);

                    if (!geoCoordsServiceResult.IsSuccess)
                    {
                        _logger.LogWarning($"Unable to get coordinates for location '{stop.Name}'.");
                    }
                    else
                    {
                        _logger.LogInformation($"Got coordinates for location '{stop.Name}'.");
                    }

                    stop.Latitude = geoCoordsServiceResult.Latitude;
                    stop.Longitude = geoCoordsServiceResult.Longitude;
                }

                await _worldRepository.AddStopsAsync(User.Identity.Name, tripId, stops);
                await _worldRepository.SaveChangesAsync();
                return NoContent();
            }
            catch (TripNotFoundException)
            {
                return NotFound();
            }

        }
    }
}
