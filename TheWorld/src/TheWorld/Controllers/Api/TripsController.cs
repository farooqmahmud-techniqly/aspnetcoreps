﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Logging;
using TheWorld.Models;
using TheWorld.ViewModels;

namespace TheWorld.Controllers.Api
{
    [Authorize]
    public class TripsController : Controller
    {
        private readonly IWorldRepository _worldRepository;
        
        public TripsController(IWorldRepository worldRepository)
        {
            _worldRepository = worldRepository;
        }

        [HttpGet("api/trips")]
        public async Task<IActionResult> Get()
        {
            var trips = await _worldRepository.GetAllTripsAsync(User.Identity.Name);
            var tripViewModels = new List<TripViewModel>();
            
            foreach (var trip in trips)
            {
                var uri = Url.Link("GetTripById", new { id = trip.Id });
                var tripViewModel = Mapper.Map<TripViewModel>(trip);
                tripViewModel.Links.Self = uri;
                tripViewModels.Add(tripViewModel);
            }

            return Ok(tripViewModels);
        }

        [HttpPost("api/trips")]
        public async Task<IActionResult> Post([FromBody]TripViewModel tripViewModel)
        {
            if (tripViewModel == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newTrip = Mapper.Map<Trip>(tripViewModel);
            newTrip.Username = User.Identity.Name;

            await _worldRepository.AddTripAsync(newTrip);
            var saved = await _worldRepository.SaveChangesAsync();

            if (!saved)
            {
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            }

            var newModel = Mapper.Map<TripViewModel>(newTrip);
            var uri = Url.Link("GetTripById", new {id = newTrip.Id});
            newModel.Links.Self = uri;
            return Created(uri, newModel);
        }

        [HttpGet("api/trips/{id}", Name = "GetTripById")]
        public async Task<IActionResult> Get(int id)
        {
            var trip = await _worldRepository.GetTripAsync(User.Identity.Name, id);

            if (trip == null)
            {
                return NotFound();
            }

            var model = Mapper.Map<TripViewModel>(trip);
            var uri = Url.Link("GetTripById", new { id = trip.Id });
            model.Links.Self = uri;
            return Ok(model);
        }

        [HttpDelete("api/trips/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _worldRepository.DeleteAsync(id);
            await _worldRepository.SaveChangesAsync();
            return NoContent();
        }
    }
}
