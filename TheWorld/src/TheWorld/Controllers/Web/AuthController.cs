﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TheWorld.Models.Identity;
using TheWorld.ViewModels;

namespace TheWorld.Controllers.Web
{
    public class AuthController : Controller
    {
        private readonly SignInManager<WorldIdentityUser> _signInManager;

        public AuthController(SignInManager<WorldIdentityUser> signInManager)
        {
            _signInManager = signInManager;
        }

        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Trips", "App");
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(
            LoginViewModel loginViewModel, 
            string redirectToUrl)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var signInResult = await _signInManager.PasswordSignInAsync(
                loginViewModel.Username,
                loginViewModel.Password,
                true,
                false);

            if (signInResult.Succeeded)
            {
                return string.IsNullOrWhiteSpace(redirectToUrl) ? 
                    RedirectToAction("Trips", "App") : 
                    RedirectToAction(redirectToUrl);
            }

            ModelState.AddModelError("", "Username or password is incorrect.");
            return View();
        }

        public async Task<IActionResult> Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                await _signInManager.SignOutAsync();
            }

            return RedirectToAction("Index", "App");
        }
    }
}
