﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using TheWorld.Models;
using TheWorld.Services;
using TheWorld.ViewModels;

namespace TheWorld.Controllers.Web
{
    public class AppController : Controller
    {
        private readonly IEmailService _emailService;
        private readonly IConfigurationRoot _configurationRoot;
        private readonly IWorldRepository _worldRepository;
        private readonly ILogger<AppController> _logger;

        public AppController(
            IEmailService emailService, 
            IConfigurationRoot configurationRoot, 
            IWorldRepository worldRepository,
            ILogger<AppController> logger)
        {
            _emailService = emailService;
            _configurationRoot = configurationRoot;
            _worldRepository = worldRepository;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            return View();
        }

        [Authorize]
        public async Task<IActionResult> Trips()
        {
            try
            {
                var trips = await _worldRepository.GetAllTripsAsync(User.Identity.Name);
                return View(trips);
            }
            catch (Exception exception)
            {
                _logger.LogError($"Error while retrieving trips.\n{exception}");
                return Redirect("/error");
            }
        }

        public IActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Contact(ContactViewModel model)
        {
            if (model.Email.Contains("aol.com"))
            {
                ModelState.AddModelError("Email", "We don't support aol.com email addresses.");
            }

            if (!ModelState.IsValid)
            {
                return View();
            }

            var toAddress = _configurationRoot["MailSettings:ToAddress"];
            _emailService.Send(toAddress, model.Email, "From The World", model.Message);
            ViewBag.UserMessage = "Message sent.";
            ModelState.Clear();
            return View();
        }

        public IActionResult About()
        {
            return View();
        }
    }
}
