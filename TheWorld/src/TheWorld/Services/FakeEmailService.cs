﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheWorld.Services
{
    public class FakeEmailService : IEmailService
    {
        public void Send(string to, string from, string subject, string body)
        {
            Debug.WriteLine($"Sending mail from \"{from}\" to \"{to}\" with subject \"{subject}\" and body \"{body}\"");
        }
    }
}
