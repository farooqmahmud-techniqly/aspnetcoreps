using System.Collections.Generic;
using System.Threading.Tasks;

namespace TheWorld.Models
{
    public interface IWorldRepository
    {
        Task<IEnumerable<Trip>> GetAllTripsAsync(string username);
        Task<Trip> GetTripAsync(string username, int id);
        Task AddTripAsync(Trip trip);
        Task<bool> SaveChangesAsync();
        Task DeleteAsync(int id);
        Task AddStopsAsync(string username, int tripId, IEnumerable<Stop> stops);
    }
}