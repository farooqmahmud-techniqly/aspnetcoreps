﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace TheWorld.Models
{
    public class WorldDataContext : DbContext
    {
        private readonly IConfigurationRoot _configurationRoot;

        public WorldDataContext(IConfigurationRoot configurationRoot, DbContextOptions<WorldDataContext> options) : base(options)
        {
            _configurationRoot = configurationRoot;
        }

        public DbSet<Trip> Trips { get; set; }
        public DbSet<Stop> Stops { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(_configurationRoot["ConnectionStrings:WorldDataContextConnection"]);
        }
    }
}
