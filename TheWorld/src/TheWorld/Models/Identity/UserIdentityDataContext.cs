﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace TheWorld.Models.Identity
{
    public class UserIdentityDataContext : IdentityDbContext<WorldIdentityUser>
    {
        private readonly IConfigurationRoot _configurationRoot;

        public UserIdentityDataContext(IConfigurationRoot configurationRoot, DbContextOptions<UserIdentityDataContext> options) : base(options)
        {
            _configurationRoot = configurationRoot;
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(_configurationRoot["ConnectionStrings:WorldUserIdentityDataContextConnection"]);
        }
    }
}
