﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace TheWorld.Models.Identity
{
    public class UserIdentityContextSeedData
    {
        private readonly UserIdentityDataContext _dataContext;
        private readonly UserManager<WorldIdentityUser> _userManager;

        public UserIdentityContextSeedData(
            UserIdentityDataContext dataContext,
            UserManager<WorldIdentityUser> userManager)
        {
            _dataContext = dataContext;
            _userManager = userManager;
        }

        public async Task EnsureSeedData()
        {
            var email = "ardonis.villareal@foo.com";

            if (await _userManager.FindByEmailAsync(email) == null)
            {
                var user = new WorldIdentityUser
                {
                    UserName = "ardonisv",
                    Email = email
                };

                var password = "P@ssword1!";
                await _userManager.CreateAsync(user, password);
            }
        }
    }
}
    