﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NuGet.Packaging;
using TheWorld.Exceptions;

namespace TheWorld.Models
{
    public class WorldRepository : IWorldRepository
    {
        private readonly WorldDataContext _dataContext;
        private readonly ILogger<WorldRepository> _logger;

        public WorldRepository(WorldDataContext dataContext, ILogger<WorldRepository> logger)
        {
            _dataContext = dataContext;
            _logger = logger;
        }

        public async Task<IEnumerable<Trip>> GetAllTripsAsync(string username)
        {
            var trips = await _dataContext.Trips.Where(t => t.Username == username).ToListAsync();
            _logger.LogInformation($"Retrieved {trips.Count} trips.");
            return trips;
        }

        public async Task<Trip> GetTripAsync(string username, int id)
        {
            return await _dataContext.Trips
                .Include(t => t.Stops)
                .Where(t => t.Username == username)
                .FirstOrDefaultAsync(t => t.Id == id);
        }

        public async Task AddTripAsync(Trip trip)
        {
            _dataContext.Add(trip);
        }

        public async Task<bool> SaveChangesAsync()
        {
           var result = await _dataContext.SaveChangesAsync();
           return result > 0;
        }

        public async Task DeleteAsync(int id)
        {
            var trip = await _dataContext.Trips.Include(t => t.Stops).SingleOrDefaultAsync(t => t.Id == id);

            if (trip == null)
            {
                return;
            }
            
            _dataContext.Remove(trip);
        }

        public async Task AddStopsAsync(string username, int tripId, IEnumerable<Stop> stops)
        {
            var trip = await GetTripAsync(username, tripId);

            if (trip == null)
            {
                throw new TripNotFoundException();
            }

            var stopList = stops.ToList();
            trip.Stops.AddRange(stopList);
            _dataContext.Stops.AddRange(stopList);
        }

        public async Task<IEnumerable<Trip>> GetAllTripsForUserAsync(string username)
        {
            var trips = await _dataContext.Trips.Where(t => t.Username == username).ToListAsync();
            return trips;
        }
    }
}