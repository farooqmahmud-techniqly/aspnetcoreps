﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TheWorld.ViewModels
{
    public class StopViewModel
    {
        [Required]
        [StringLength(100, MinimumLength = 5)]
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        [Required]
        [Range(0, 99)]
        public int Order { get; set; }

        public DateTime Arrival { get; set; }
    }
}