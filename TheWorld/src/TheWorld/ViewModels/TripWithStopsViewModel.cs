﻿using System.Collections.Generic;

namespace TheWorld.ViewModels
{
    public class TripWithStopsViewModel : ViewModelBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<StopViewModel> Stops { get; set; }
    }
}