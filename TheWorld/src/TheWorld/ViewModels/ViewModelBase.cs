﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheWorld.ViewModels
{
    public abstract class ViewModelBase
    {
        protected ViewModelBase()
        {
            Links = new Links();
        }

        public Links Links { get; set; }
    }
}
