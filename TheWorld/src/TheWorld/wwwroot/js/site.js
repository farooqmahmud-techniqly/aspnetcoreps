﻿(function () {

    //var usernameElement = $("#username");
    //usernameElement.text("Farooq Mahmud");

    //var mainElement = $("#main");

    //mainElement.on("mouseenter", function () {
    //    mainElement.style = "background-color: #a9a9a9;";
    //});

    //mainElement.on("mouseleave",
    //    function() {
    //        mainElement.style = "";
    //    });

    //var menuItems = $("ul.menu li a");

    //menuItems.on("click",
    //    function () {
    //        var me = $(this);
    //        alert(me.text());
    //    });

    var $sideBarAndWrapper = $("#sidebar, #wrapper");
    var $icon = $("#toggle_menu i.fa");

    $("#toggle_menu")
        .on("click",
            function() {
                $sideBarAndWrapper.toggleClass("hide-sidebar");

                if ($sideBarAndWrapper.hasClass("hide-sidebar")) {
                    $icon.removeClass("fa-angle-left");
                    $icon.addClass("fa-angle-right");
                } else {
                    $icon.removeClass("fa-angle-right");
                    $icon.addClass("fa-angle-left");
                }
            });

})();